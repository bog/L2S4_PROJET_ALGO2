CC= gcc
CFLAGS= -Wall -std=c99 -g 

all: player-ossete_gombe-dumontet referee-ossete_gombe-dumontet

player-ossete_gombe-dumontet: player-ossete_gombe-dumontet.o stratego-ossete_gombe-dumontet.o
	$(CC) $(CFLAGS) $^ -o $@

referee-ossete_gombe-dumontet: referee-ossete_gombe-dumontet.o stratego-ossete_gombe-dumontet.o
	$(CC) $(CFLAGS) $^ -o $@

stratego-ossete_gombe-dumontet: stratego-ossete_gombe-dumontet.o
	$(CC) -c $(CFLAGS) $^ -o $@

clean:
	rm -rf *.o

mrproper: clean
	rm -rf player-ossete_gombe-dumontet referee-ossete_gombe-dumontet stratego-ossete_gombe-dumontet
