#define _POSIX_C_SOURCE 1
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include "stratego-ossete_gombe-dumontet.h"
#include "player-ossete_gombe-dumontet.h"
#include "ai_param-ossete_gombe-dumontet.h"

int main(int argc, char** argv)
{
  /* INIT */
  srand(time(NULL));
  // initialisation of the game
  int num_player = -1;
  struct game_state game; 
  game.pieces_count = 0;
  
  /* GET THE PLAYER NUMBER */
  char tmp[BUFSIZE];
  if( read_one_line(tmp) )
    {
      num_player = atoi( tmp ); // TODO strtol pour etre sure

      // invalid num player
      if( num_player != 0 && num_player != 1 )
	{
	  fprintf(stderr, "E: Can't read my player number ! (what is %d ?)\n",num_player);
	  exit(EXIT_FAILURE);
	}

      if(DEBUG_MODE){ fprintf(stderr, "D: Got the number %d !\n", num_player);}//DEBUG
    }

  /* PLACE ALL THE PIECES */
  // init an empty board
  board_init_empty(&game.my_board);
  // place my own pieces
  place(num_player, &game);
  // place enemy's pieces
  place_enemy(num_player, &game);
  
  /* SEND STRATEGY TO REFEREE */
  send_place(num_player, &game.my_board);
    
  /* WAIT FOR VALIDATION OF THE STRATEGY */
  if( read_one_line(tmp) )
    {
      if( strcmp(tmp, "KO") == 0 )
	{
	  fprintf(stderr,"E: Board hasn't been accepted !\n");
	  exit(EXIT_FAILURE);
	}
      else if( strcmp(tmp, "OK") != 0 )
	{
	  fprintf(stderr,"E: I don't know if the board has been accepted or not ! (what is %s ?)\n", tmp);
	  exit(EXIT_FAILURE);
	}
    }
  else
    {
      fprintf(stderr,"E: while reading referee answer !\n");
      exit(EXIT_FAILURE);
    } 
  
  
  /** ########## START OF THE GAME ########## **/

  //  board_display( &myboard );//DEBUG DISPLAY

  int turn_number = 1;
  
  // the player 1 have to wait for player 0's first turn
  if( num_player == 1 )
    {
      his_turn(num_player, &game);
    }
  
  // game loop
  while( 1 )
    {
      if( DEBUG_MODE ){fprintf(stderr, "\n---------- TURN %d----------\n", turn_number);}
      
      turn_number++;
      //      board_display( &game.my_board );//DEBUG DISPLAY
      if( !my_turn(num_player, &game) )
	{
	  cant_play(num_player);
	  exit(EXIT_FAILURE);
	}

      //      board_display( &game.my_board );//DEBUG DISPLAY
      if( !his_turn(num_player, &game) )
	{
	  cant_read();
	  exit(EXIT_FAILURE);
	}

      // update the probabilities of each ennemy's troop
      probabilities_update(num_player, &game);
    }
    
  return 0;
}

int place(int num_player, struct game_state *game)
{
  // positionnement strategy
  int pieces_array[10][4] =
    {
      {CONTENT_SCOUT,CONTENT_SERGENT,CONTENT_LIEUTENANT,CONTENT_CAPTAIN},
      {CONTENT_MINESWEEPER,CONTENT_BOMB,CONTENT_SERGENT,CONTENT_SCOUT},
      {CONTENT_BOMB,CONTENT_SERGENT,CONTENT_BOMB,CONTENT_SCOUT},
      {CONTENT_SCOUT,CONTENT_COMMANDER,CONTENT_SPY,CONTENT_LIEUTENANT},
      {CONTENT_MINESWEEPER,CONTENT_COLONEL,CONTENT_GENERAL,CONTENT_SCOUT},
      {CONTENT_BOMB,CONTENT_LIEUTENANT,CONTENT_SCOUT,CONTENT_CAPTAIN},
      {CONTENT_FLAG,CONTENT_BOMB,CONTENT_COMMANDER,CONTENT_MINESWEEPER},
      {CONTENT_BOMB,CONTENT_LIEUTENANT,CONTENT_COMMANDER,CONTENT_CAPTAIN},
      {CONTENT_MINESWEEPER,CONTENT_CAPTAIN,CONTENT_COLONEL,CONTENT_SCOUT},
      {CONTENT_MINESWEEPER,CONTENT_SERGENT,CONTENT_SCOUT,CONTENT_MARSHAL}
    };
    
  // place our troops in the board to fit with the positionnement strategy
  for(int i=0; i<10; i++)
    {
      for(int j=0; j<4; j++)
	{
	  place_one_content(num_player, &game->my_board, pieces_array[i][j], SYM(i) , SYM(j) );

	  // init probabilities
	  for(int k=0; k< PIECE_COUNT; k++)
	    {
	      if( game->my_board.data[SYM(i)][SYM(j)].content == k )
		{
		  game->my_board.data[SYM(i)][SYM(j)].probabilities[k] = 1;
		}
	      else
		{
		  game->my_board.data[SYM(i)][SYM(j)].probabilities[k] = 0;
		}
	    } 
	}
    }

  // init the count of my own pieces
  for(int i=0; i< PIECE_COUNT; i++)
    {
      game->my_pieces[i] = quantity_rule[i];
    }
  game->my_pieces[CONTENT_UNKNOWN] = -1;
  game->my_pieces[CONTENT_NONE] = -1;
  
  return 1;
}

// placement of ennemy's troop on my own board
int place_enemy(int num_player, struct game_state *game)
{
  for(int i=0; i<10; i++)
    {
      for(int j=0; j<4; j++)
	{
	  // the ennemy can't be placed here
	  if( !place_one_content( OTHER(num_player) , &game->my_board, CONTENT_UNKNOWN, SYM_OTHER(i), SYM_OTHER(j) ) )
	    {
	      return 0;
	    }

	  game->pieces_count++;
	  // init probabilities for the ennemy troop just placed
	  for(int k=0; k< PIECE_COUNT; k++)
	    {
	      game->my_board.data[SYM_OTHER(i)][SYM_OTHER(j)].probabilities[k] = quantity_rule[k]/40.0;
	    }
	}
    }

  // memorise that we don't know any content of opponent's pieces
  for(int i=0; i< PIECE_COUNT; i++)
    {
      game->his_pieces[i] = quantity_rule[i];
      game->discovered_pieces[i] = 0;
    }
  
    game->his_pieces[CONTENT_UNKNOWN] = -1;
    game->his_pieces[CONTENT_NONE] = -1;
    
  return 1;
}

// send the placement of my troops to the referee
void send_place(int num_player, struct board* board)
{
  // my placement is not valid
  if( !check_place(num_player, board) )
    {
      fprintf(stderr,"E: invalid initialized board !\n");
      exit(EXIT_FAILURE); // TODO retry (init the board)
    }
  
  // send the board
  for(int i=0; i < BOARD_HEIGHT; i++)
    {
      for(int j=0; j < BOARD_WIDTH; j++)
	{
	  if( board->data[i][j].team == num_player )
	    {
	      //printf("%d\n", board->data[i][j].content); DEBUG PRINTF
	      char to_send[BUFSIZE];
	      snprintf(to_send, BUFSIZE,"%d\n", board->data[i][j].content);
	      write_out(to_send);
	    }
	}
    }
}

// display a message on stderr if the read fails
void cant_read()
{
  fprintf(stderr,"E: can't listen the answer !\n");
}

// display a message on stderr if the move fails
void cant_play(int num_player)
{
  fprintf(stderr,"E(%d) : I can't play !\n", num_player);
}

// play my turn
int my_turn(int num_player, struct game_state *game)
{
  char my_step[BUFSIZE];
  char my_answer[BUFSIZE];

  /* COMPUTE NEXT STEP */
  next_step(num_player, game, my_step);

  /* SEND NEXT STEP */
  write_out(my_step);

  /* WAIT FOR REFEREE'S ANSWER */
  if( !read_one_line(my_answer) )
    {
      cant_read();
    }

  /* UPDATE BOARD AFTER MY STEP */
  // Failure of the opponent
  if( strcmp(my_answer, "F") == 0 )
    {
      //TODO something here
    }
  // Failure of the player
  else if( strcmp(my_answer, "E") == 0 )
    {
      return 0;
    }
  // Move without battle
  else if( strcmp(my_answer, "N") == 0 )
    {
      // move not valid
      if( !move_one_content(num_player,&game->my_board, my_step[0]-'A', my_step[1]-'0', my_step[3], my_step[5]-'0') )
	{
	  fprintf(stderr, "E: Can't understand my move !\n");
	  return 0;
	}
    }
  // Battle !!!
  else if( strcmp(my_answer, "B") == 0 )
    {
      // get attack value from referee
      char battle_attack[BUFSIZE];
      if( !read_one_line(battle_attack) )
	{
	  cant_read();
	}
      int value_attack = atoi(battle_attack);
      
      // get defend value from referee
      char battle_defend[BUFSIZE];
      if( !read_one_line(battle_defend) )
	{
	  cant_read();
	}
      int value_defend = atoi(battle_defend);
	 
      // compare the opponents and store the result of the fight in battle_result
      int battle_result = piece_cmp(value_attack, value_defend);
      
      // position of the attacker
      int old_i = my_step[0]-'A';
      int old_j = my_step[1]-'0';

      // compute the position of the defender
      int def_i = 0;
      int def_j = 0;
      get_destination(old_i, old_j, &def_i, &def_j, my_step[3], my_step[5]-'0');
	  
      // I win the fight (attacker better than defender)
      if( battle_result > 0 )
	{
	  // if we used to know the opponent, we remove him from the discovered pieces
	  if( game->my_board.data[def_i][def_j].content != CONTENT_UNKNOWN )
	    {
	      game->discovered_pieces[ value_defend ]--;
	    }
	    
	  game->his_pieces[ value_defend ]--;
	  game->pieces_count--;

	  // remove defender
	  reset_tile(&game->my_board, def_i, def_j);
	  
	  // move attacker to its destination
	  if( !move_one_content(num_player,&game->my_board, old_i, old_j, my_step[3], my_step[5]-'0') )
	    {
	      fprintf(stderr, "E: Can't battle this way !\n");
	      return 0;
	    } 
	}
      // I lose the fight (defender better than attacker)
      else if( battle_result < 0 )
	{
	  game->my_pieces[ value_attack ]--;

	  // if the opponent was unknown, then memorise it
	  if( game->my_board.data[def_i][def_j].content == CONTENT_UNKNOWN )
	    {
	      game->discovered_pieces[ value_defend ]++;
	    }

	  // update board
	  game->my_board.data[def_i][def_j].content = value_defend;

	  // remove attacker
	  reset_tile(&game->my_board, old_i, old_j);
	}
      // it is a draw (attacker == defender)
      else
	{
	  // if we used to know the opponent, we remove him from the discovered pieces
	  if( game->my_board.data[def_i][def_j].content != CONTENT_UNKNOWN )
	    {
	      game->discovered_pieces[ value_defend ]--;
	    }
	  
	  game->my_pieces[ value_attack ]--;
	  game->his_pieces[ value_defend ]--;
	  game->pieces_count--;

	  // remove attacker and defender
	  reset_tile(&game->my_board, old_i, old_j);
	  reset_tile(&game->my_board, def_i, def_j);
	} 
    }
  // I read something else than N, B, E or F
  else
    {
      fprintf(stderr,"E(%d) : I don't understand that dumb referre ! I quit ! (my answer is %s ?)\n",num_player, my_answer);
      return 0;
    }

  return 1;
    
}

// play the opponent turn
int his_turn(int num_player, struct game_state *game)
{
  char tmp[BUFSIZE];

  // read the position
  if( !read_one_line(tmp) )
    {
      cant_read();
    }      

  int old_i = tmp[0]-'A';
  int old_j = tmp[1]-'0';

  // read the the direction
  if( !read_one_line(tmp) )
    {
      cant_read();
    }

  char enemy_dir = tmp[0];

  // read the distance
  if( !read_one_line(tmp) )
    {
      cant_read();
    }
  int enemy_dist = atoi(tmp);
  
  //fprintf(stderr, "D(%d) : enemy step %d %d %c %d\n", num_player, old_i, old_j, enemy_dir, enemy_dist);//DEBUG

  // compute destination's coordinates
  int new_i = 0;
  int new_j = 0;
  get_destination(old_i, old_j, &new_i, &new_j, enemy_dir, enemy_dist);

  /* WAIT FOR REFEREE'S ANSWER */
  char his_answer[BUFSIZE];
  
  if( !read_one_line(his_answer) )
    {
      cant_read();
    }

  /* UPDATE BOARD AFTER HIS STEP */
  // My failure
  if( strcmp(his_answer, "F") == 0 )
    {
      return 0;
    }
  
  // failure of the enemy
  else if( strcmp(his_answer, "E") == 0 )
    {
      //TODO something here
    }
  
  // move without battle
  else if( strcmp(his_answer, "N") == 0 )
    {
      //printf("---> %d %d %c %d\n", old_i, old_j, enemy_dir, enemy_dist);//DEBUG

      // it is not a bomb nor a flag
      game->my_board.data[old_i][old_j].has_moved = 1;
      
      if( enemy_dist > 1 ) // it is a scout
	{
	  // if we didn't know him, now we do.
	  if( game->my_board.data[old_i][old_j].content == CONTENT_UNKNOWN )
	    {
	      game->discovered_pieces[ CONTENT_SCOUT ]++;
	    }

	  // memorize that it is a scout
	  game->my_board.data[old_i][old_j].content = CONTENT_SCOUT;
	}

      // move the enemy
      if( !move_one_content( OTHER(num_player),&game->my_board, old_i, old_j, enemy_dir, enemy_dist) )
	{
	  fprintf(stderr, "E: Can't understand his move !\n");
	}
      
    }
  // battle
  else if( strcmp(his_answer, "B") == 0 )
    {
      // get the attacker value from the referee
      char battle_attack[BUFSIZE];
      if( !read_one_line(battle_attack) )
	{
	  cant_read();
	}
      int value_attack = atoi(battle_attack);
      
      // get the defender value from the referee
      char battle_defend[BUFSIZE];
      if( !read_one_line(battle_defend) )
	{
	  cant_read();
	}
      int value_defend = atoi(battle_defend);

      // compare attacker with defender
      int battle_result = piece_cmp(value_attack, value_defend);
	  
      int def_i = 0;
      int def_j = 0;

      get_destination(old_i, old_j, &def_i, &def_j, enemy_dir, enemy_dist);

      // the opponent found the flag : he won
      if(value_defend == CONTENT_FLAG)
	{
	  return 0;
	}
      
      // I lost (attaker better than defender)
      else if( battle_result > 0 )
	{
	  // if we used to don't know him, we add him to the discovered pieces
	  if( game->my_board.data[old_i][old_j].content == CONTENT_UNKNOWN )
	    {
	      game->discovered_pieces[ value_attack ]++;
	    }
	  
	  game->my_pieces[ value_defend ]--;
	  
	  // remove defender
	  reset_tile(&game->my_board, def_i, def_j);

	  // move the ennemy
	  if( !move_one_content( OTHER(num_player),&game->my_board, old_i, old_j, enemy_dir, enemy_dist) )
	    {
	      if( DEBUG_MODE ){ fprintf(stderr, "D : %c %d %c %d\n", old_i + 'A', old_j, enemy_dir, enemy_dist); }
	      fprintf(stderr, "E: Can't report battle result !\n");
	      return 0;
	    }
	  
	  //update board with a new friend
	  game->my_board.data[def_i][def_j].content = value_attack;
	  game->my_board.data[def_i][def_j].has_moved = 1;
		  
	}
      // I won (defender better than attaker)
      else if( battle_result < 0 )
	{
	  // if we used to know him, we remove him from the discovered pieces
	  if( game->my_board.data[old_i][old_j].content != CONTENT_UNKNOWN )
	    {
	      game->discovered_pieces[ value_attack ]--;
	    }
	  
	  game->his_pieces[ value_attack ]--;
	  game->pieces_count--;

	  // remove attacker
	  reset_tile(&game->my_board, old_i, old_j); 
	}
      // draw (defender as strong as attacker)
      else
	{
	  // if we used to know him, we remove him from the discovered pieces
	  if( game->my_board.data[old_i][old_j].content != CONTENT_UNKNOWN )
	    {
	      game->discovered_pieces[ value_attack ]--;
	    }
	  
	  game->my_pieces[ value_defend ]--;
	  game->his_pieces[ value_attack ]--;
	  game->pieces_count--;
	  
	  // remove both pieces
	  reset_tile(&game->my_board, old_i, old_j);
	  reset_tile(&game->my_board, def_i, def_j);
	}
    }
  else
    {
      fprintf(stderr,"E(%d) : I don't understand that dumb referre ! I quit ! (his answer is %s ?)\n",num_player, his_answer);
      return 0;
    }
  
  return 1;
}

// our dumb player
int next_step_debug(int num_player, struct game_state *game, char* res)
{
  char dir = EAST;
  int dist = 1;
  
  for(int i=0; i< BOARD_HEIGHT; i++)
    {
      for(int j=0; j< BOARD_WIDTH; j++)
	{
	  if( move_is_valid(num_player, &game->my_board, i, j, dir, dist) )
	    {
	      sprintf(res, "%c%d\n%c\n%d\n",'A'+i, j,dir,dist);
	      return 1;
	    }
	}
    }
  
  dir = WEST;
  for(int i=0; i< BOARD_HEIGHT; i++)
    {
      for(int j=0; j< BOARD_WIDTH; j++)
	{
	  if( move_is_valid(num_player, &game->my_board, i, j, dir, dist) )
	    {
	      sprintf(res, "%c%d\n%c\n%d\n",'A'+i, j,dir,dist);
	      return 1;
	    }
	}
    }
  

  fprintf(stderr,"E: I can't do anything !\n");
  
  return 0;
  
}


// compute the next step, fill res with the choosen step,
// return 0 if he failed to compute it
int next_step(int num_player, struct game_state *game, char* res)
{
  suseconds_t initial = get_microseconds();
 
  int i = 0;
  int j = 0;

  int new_value = 0;// last computed step
  
  char directions[4]= {NORTH, EAST, SOUTH, WEST};
  int current_direction = 0;

  int next_position = 0;
  
  int value = -1;
  int dist = 1;
  char dir = directions[current_direction];

  while( get_microseconds() - initial < STEP_TIME_MAX && i < BOARD_HEIGHT)
    {
      /// POSITION (I, J)
      if( next_position == 0 )
	{
	  /* evaluate position */
	  dir = directions[current_direction];
	  if( move_is_valid(num_player, &game->my_board, i, j, dir, dist) )
	    {
	      //fprintf(stderr, "D(%d) : %d %d %c %d \n", num_player, i, j, dir, dist);//DEBUG
	      new_value = evaluate(num_player, game, i, j, dir, dist);
	    
	      // this step is better than the older
	      if( new_value > value )
		{
		  sprintf(res, "%c%d\n%c\n%d\n", 'A'+i, j, dir, dist);
		  // fprintf(stderr, "D(%d) : %s \n", num_player, res);//DEBUG
		  value = new_value;
		}
	      // this step is as good as the older : random between both of them
	      else if( new_value == value )
		{
		  if( (float)random(0,101)/100.0 <= STEP_RANDOM_THRESHOLD )
		    {
		      sprintf(res, "%c%d\n%c\n%d\n", 'A'+i, j, dir, dist);
		      value = new_value;
		    }
		}
	    }

	  dist++;

	  /* next thing to do (between dist++, direction++ or tile++ ) */
	  if( !move_is_valid(num_player, &game->my_board, i, j, dir, dist) )
	    {
	      current_direction++;
	      dist = 1;
	    }
	  
	  if( current_direction >= 4 )
	    {
	      current_direction = 0;
	      next_position = 1;
	    }

	}
      /// NEXT POSITION
      else
	{
	  if( j >= BOARD_WIDTH - 1 )
	    {
	      j = 0;
	      i++;
	    }
	  else
	    {
	      j++;
	    }

	  next_position = 0;
	}
      
    }
  
  // return 1 if we found a value, 0 else
  return ( value != -1 );
}

// call the right evaluate function depending on the content
int evaluate(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  switch(game->my_board.data[i][j].content)
    {
    case CONTENT_MARSHAL: return evaluate_marshal(num_player, game, i, j, dir, dist); 
    case CONTENT_GENERAL: return evaluate_general(num_player, game, i, j, dir, dist); 
    case CONTENT_COLONEL: return evaluate_colonel(num_player, game, i, j, dir, dist); 
    case CONTENT_COMMANDER: return evaluate_commander(num_player, game, i, j, dir, dist); 
    case CONTENT_CAPTAIN: return evaluate_captain(num_player, game, i, j, dir, dist); 
    case CONTENT_LIEUTENANT: return evaluate_lieutenant(num_player, game, i, j, dir, dist); 
    case CONTENT_SERGENT: return evaluate_sergent(num_player, game, i, j, dir, dist); 
    case CONTENT_MINESWEEPER: return evaluate_minesweeper(num_player, game, i, j, dir, dist); 
    case CONTENT_SCOUT: return evaluate_scout(num_player, game, i, j, dir, dist); 
    case CONTENT_SPY: return evaluate_spy(num_player, game, i, j, dir, dist);
      
    default: return evaluate_default(num_player, game, i, j, dir, dist);
    }
}


/* bellow, all the EVALUATE FUNCTIIONS */
// use ai_param-ossete_gombe-dumontet.h defines


int evaluate_marshal(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  int new_i = 0;
  int new_j = 0;
	
  get_destination(i, j, &new_i, &new_j, dir, dist);

  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      //avoid
      
      int avoid_score = avoid(num_player, game, MARSHAL_AVOID, MARSHAL_AVOID_THRESHOLD, MARSHAL_AVOID_DANGER, MARSHAL_NB_AVOID, i, j, new_i,new_j);
      
      //hunt
      int hunt_score = hunt(num_player ,game, i, j, dir, dist,
			    MARSHAL_HUNT_TARGETS, MARSHAL_HUNT_NB_TARGETS, MARSHAL_HUNT_RADIUS, MARSHAL_HUNT_THRESHOLD);

      if( avoid_score == 0 )
	{
	  return hunt_score;
	}
      else
	{
	  return (avoid_score < hunt_score) ? avoid_score : hunt_score;
	}
      
    }
  else
    {
      //FIGHT
      int defend = game->my_board.data[new_i][new_j].content;

      return EVALUATE_NEUTRAL + MARSHAL_REWARDS[defend];
    }
}


int evaluate_general(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  int new_i = 0;
  int new_j = 0;
	
  get_destination(i, j, &new_i, &new_j, dir, dist);


  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      //avoid
      
      int avoid_score = avoid(num_player, game, GENERAL_AVOID, GENERAL_AVOID_THRESHOLD, GENERAL_AVOID_DANGER, GENERAL_NB_AVOID, i, j, new_i,new_j);
      
      //hunt
      int hunt_score = hunt(num_player ,game, i, j, dir, dist,
			    GENERAL_HUNT_TARGETS, GENERAL_HUNT_NB_TARGETS, GENERAL_HUNT_RADIUS, GENERAL_HUNT_THRESHOLD);

      
      if( avoid_score == 0 )
	{
	  return hunt_score;
	}
      else
	{
	  return (avoid_score < hunt_score) ? avoid_score : hunt_score;
	}
      
    }
  else
    {
      //FIGHT
      int defend = game->my_board.data[new_i][new_j].content;

      return EVALUATE_NEUTRAL + GENERAL_REWARDS[defend];
    }
}

int evaluate_colonel(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  int new_i = 0;
  int new_j = 0;
	
  get_destination(i, j, &new_i, &new_j, dir, dist);


  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      //avoid
      
      int avoid_score = avoid(num_player, game, COLONEL_AVOID, COLONEL_AVOID_THRESHOLD, COLONEL_AVOID_DANGER, COLONEL_NB_AVOID, i, j, new_i,new_j);
      
      //hunt
      int hunt_score = hunt(num_player ,game, i, j, dir, dist,
			    COLONEL_HUNT_TARGETS, COLONEL_HUNT_NB_TARGETS, COLONEL_HUNT_RADIUS, COLONEL_HUNT_THRESHOLD);

      //fprintf(stderr, "D : avoid=%d hunt=%d coup=%c %d %c %d\n", avoid_score, hunt_score, i +'A', j, dir, dist);
      if( avoid_score == 0 )
	{
	  return hunt_score;
	}
      else
	{
	  return (avoid_score < hunt_score) ? avoid_score : hunt_score;
	}
      
    }
  else
    {
      //FIGHT
      int defend = game->my_board.data[new_i][new_j].content;

      return EVALUATE_NEUTRAL + COLONEL_REWARDS[defend];
    }

}

int evaluate_commander(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  int new_i = 0;
  int new_j = 0;
	
  get_destination(i, j, &new_i, &new_j, dir, dist);


  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      //avoid
      
      int avoid_score = avoid(num_player, game, COMMANDER_AVOID, COMMANDER_AVOID_THRESHOLD, COMMANDER_AVOID_DANGER, COMMANDER_NB_AVOID, i, j, new_i,new_j);
      
      //hunt
      int hunt_score = hunt(num_player ,game, i, j, dir, dist,
			    COMMANDER_HUNT_TARGETS, COMMANDER_HUNT_NB_TARGETS, COMMANDER_HUNT_RADIUS, COMMANDER_HUNT_THRESHOLD);

      //fprintf(stderr, "D : avoid=%d hunt=%d coup=%c %d %c %d\n", avoid_score, hunt_score, i +'A', j, dir, dist);
      if( avoid_score == 0 )
	{
	  return hunt_score;
	}
      else
	{
	  return (avoid_score < hunt_score) ? avoid_score : hunt_score;
	}
      
    }
  else
    {
      //FIGHT
      int defend = game->my_board.data[new_i][new_j].content;

      return EVALUATE_NEUTRAL + COMMANDER_REWARDS[defend];
    }
}

int evaluate_captain(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  int new_i = 0;
  int new_j = 0;
	
  get_destination(i, j, &new_i, &new_j, dir, dist);


  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      //avoid
      
      int avoid_score = avoid(num_player, game, CAPTAIN_AVOID, CAPTAIN_AVOID_THRESHOLD, CAPTAIN_AVOID_DANGER, CAPTAIN_NB_AVOID, i, j, new_i,new_j);
      
      //hunt
      int hunt_score = hunt(num_player ,game, i, j, dir, dist,
			    CAPTAIN_HUNT_TARGETS, CAPTAIN_HUNT_NB_TARGETS, CAPTAIN_HUNT_RADIUS, CAPTAIN_HUNT_THRESHOLD);

      //fprintf(stderr, "D : avoid=%d hunt=%d coup=%c %d %c %d\n", avoid_score, hunt_score, i +'A', j, dir, dist);
      if( avoid_score == 0 )
	{
	  return hunt_score;
	}
      else
	{
	  return (avoid_score < hunt_score) ? avoid_score : hunt_score;
	}
      
    }
  else
    {
      //FIGHT
      int defend = game->my_board.data[new_i][new_j].content;

      return EVALUATE_NEUTRAL + CAPTAIN_REWARDS[defend];
    }
}

int evaluate_lieutenant(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  int new_i = 0;
  int new_j = 0;
	
  get_destination(i, j, &new_i, &new_j, dir, dist);


  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      //avoid
      
      int avoid_score = avoid(num_player, game, LIEUTENANT_AVOID, LIEUTENANT_AVOID_THRESHOLD, LIEUTENANT_AVOID_DANGER, LIEUTENANT_NB_AVOID, i, j, new_i,new_j);
      
      //hunt
      int hunt_score = hunt(num_player ,game, i, j, dir, dist,
			    LIEUTENANT_HUNT_TARGETS, LIEUTENANT_HUNT_NB_TARGETS, LIEUTENANT_HUNT_RADIUS, LIEUTENANT_HUNT_THRESHOLD);

      //fprintf(stderr, "D : avoid=%d hunt=%d coup=%c %d %c %d\n", avoid_score, hunt_score, i +'A', j, dir, dist);
      if( avoid_score == 0 )
	{
	  return hunt_score;
	}
      else
	{
	  return (avoid_score < hunt_score) ? avoid_score : hunt_score;
	}
      
    }
  else
    {
      //FIGHT
      int defend = game->my_board.data[new_i][new_j].content;

      return EVALUATE_NEUTRAL + LIEUTENANT_REWARDS[defend];
    }
}

int evaluate_sergent(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  int new_i = 0;
  int new_j = 0;
	
  get_destination(i, j, &new_i, &new_j, dir, dist);


  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      //avoid
      
      int avoid_score = avoid(num_player, game, SERGENT_AVOID, SERGENT_AVOID_THRESHOLD, SERGENT_AVOID_DANGER, SERGENT_NB_AVOID, i, j, new_i,new_j);
      
      //hunt
      int hunt_score = hunt(num_player ,game, i, j, dir, dist,
			    SERGENT_HUNT_TARGETS, SERGENT_HUNT_NB_TARGETS, SERGENT_HUNT_RADIUS, SERGENT_HUNT_THRESHOLD);

      //fprintf(stderr, "D : avoid=%d hunt=%d coup=%c %d %c %d\n", avoid_score, hunt_score, i +'A', j, dir, dist);
      if( avoid_score == 0 )
	{
	  return hunt_score;
	}
      else
	{
	  return (avoid_score < hunt_score) ? avoid_score : hunt_score;
	}
      
    }
  else
    {
      //FIGHT
      int defend = game->my_board.data[new_i][new_j].content;

      return EVALUATE_NEUTRAL + SERGENT_REWARDS[defend];
    }
}

int evaluate_minesweeper(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  int new_i = 0;
  int new_j = 0;
	
  get_destination(i, j, &new_i, &new_j, dir, dist);


  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      //avoid
      
      int avoid_score = avoid(num_player, game, MINESWEEPER_AVOID, MINESWEEPER_AVOID_THRESHOLD, MINESWEEPER_AVOID_DANGER, MINESWEEPER_NB_AVOID, i, j, new_i,new_j);
      
      //hunt
      int hunt_score = hunt(num_player ,game, i, j, dir, dist,
			    MINESWEEPER_HUNT_TARGETS, MINESWEEPER_HUNT_NB_TARGETS, MINESWEEPER_HUNT_RADIUS, MINESWEEPER_HUNT_THRESHOLD) + 100 * (game->pieces_count <= 10);

      //fprintf(stderr, "D : MINESWEEPER avoid=%d hunt=%d coup=%c %d %c %d\n", avoid_score, hunt_score, i +'A', j, dir, dist);
      
      if( avoid_score == 0 )
	{
	  return hunt_score;
	}
      else
	{
	  return (avoid_score < hunt_score) ? avoid_score : hunt_score;
	}
      
    }
  else
    {
      //FIGHT
      int defend = game->my_board.data[new_i][new_j].content;
      
      if( game->pieces_count <= 10 )
	{
	  return EVALUATE_NEUTRAL + 1500;
	}
      
      return EVALUATE_NEUTRAL + MINESWEEPER_REWARDS[defend];
    }
}

int evaluate_scout(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  int new_i = 0;
  int new_j = 0;
	
  get_destination(i, j, &new_i, &new_j, dir, dist);


  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      //avoid
      
      int avoid_score = avoid(num_player, game, SCOUT_AVOID, SCOUT_AVOID_THRESHOLD, SCOUT_AVOID_DANGER, SCOUT_NB_AVOID, i, j, new_i,new_j);
      
      //hunt
      int hunt_score = hunt(num_player ,game, i, j, dir, dist,
			    SCOUT_HUNT_TARGETS, SCOUT_HUNT_NB_TARGETS, SCOUT_HUNT_RADIUS, SCOUT_HUNT_THRESHOLD);

      //fprintf(stderr, "D : avoid=%d hunt=%d coup=%c %d %c %d\n", avoid_score, hunt_score, i +'A', j, dir, dist);
      if( avoid_score == 0 )
	{
	  return hunt_score;
	}
      else
	{
	  return (avoid_score < hunt_score) ? avoid_score : hunt_score;
	}
      
    }
  else
    {
      //FIGHT
      int defend = game->my_board.data[new_i][new_j].content;

      return EVALUATE_NEUTRAL + SCOUT_REWARDS[defend];
    }
}

int evaluate_spy(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{
  int new_i = 0;
  int new_j = 0;
	
  get_destination(i, j, &new_i, &new_j, dir, dist);


  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      //avoid
      
      int avoid_score = avoid(num_player, game, SPY_AVOID, SPY_AVOID_THRESHOLD, SPY_AVOID_DANGER, SPY_NB_AVOID, i, j, new_i,new_j);
      
      //hunt
      int hunt_score = hunt(num_player ,game, i, j, dir, dist,
			    SPY_HUNT_TARGETS, SPY_HUNT_NB_TARGETS, SPY_HUNT_RADIUS, SPY_HUNT_THRESHOLD);

      //fprintf(stderr, "D : avoid=%d hunt=%d coup=%c %d %c %d\n", avoid_score, hunt_score, i +'A', j, dir, dist);
      if( avoid_score == 0 )
	{
	  return hunt_score;
	}
      else
	{
	  return (avoid_score < hunt_score) ? avoid_score : hunt_score;
	}
      
    }
  else
    {
      //FIGHT
      int defend = game->my_board.data[new_i][new_j].content;

      return EVALUATE_NEUTRAL + SPY_REWARDS[defend];
    }
}


int evaluate_default(int num_player , struct game_state *game , int i ,int j ,char dir, int dist)
{

  int new_i = 0;
  int new_j = 0;

  get_destination(i, j, &new_i, &new_j, dir, dist);

  //MOVE
  if( game->my_board.data[new_i][new_j].content == CONTENT_NONE )
    {
      if( (num_player == 0 && new_j > j) || (num_player == 1 && new_j < j))
	{
	  return EVALUATE_NEUTRAL + 1;
	}
      if( (num_player == 0 && new_j < j) || (num_player == 1 && new_j > j))
	{
	  return EVALUATE_NEUTRAL - 1;
	}
      
      return EVALUATE_NEUTRAL;
    }

  //FIGHT
  int attack = game->my_board.data[i][j].content;
  int defend = game->my_board.data[new_i][new_j].content;
  
  //unknown result
  if( defend == CONTENT_UNKNOWN )
    {
      return EVALUATE_NEUTRAL + 2;
    }
  //predictable result
  else
    {
      int battle_result = piece_cmp(attack, defend);

      int reward[PIECE_COUNT] =
	{
	  -1,
	  REWARD_MARSHAL,
	  REWARD_GENERAL,
	  REWARD_COLONEL,
	  REWARD_COMMANDER,
	  REWARD_CAPTAIN,
	  REWARD_LIEUTENANT,
	  REWARD_SERGENT,
	  REWARD_MINESWEEPER,
	  REWARD_SCOUT,
	  REWARD_SPY,
	  REWARD_FLAG,
	  REWARD_BOMB,
	  -1,
	  -1
	};
      
      //you win
      if( battle_result > 0 )
	{
	  return EVALUATE_NEUTRAL + reward[defend];
	}
      //you loose
      else if( battle_result < 0 )
	{
	  return EVALUATE_NEUTRAL - reward[attack];
	}
      //draw
      else
	{
	  return EVALUATE_NEUTRAL + 5;//TODO dfferent valuation from a move without battle
	}

    }
  
  return -1;
}


// update the probabilities of all the tiles of the board
void probabilities_update(int num_player, struct game_state *game)
{
  assert(game);
  assert(num_player == 0 || num_player == 1);
  
  int something_discovered = 0;

  /* COUNT PIECES */
  int sum_pieces = game->pieces_count;
  int sum_pieces_known = 0;

  // count the known pieces in the board
  for(int k=CONTENT_MARSHAL; k<=CONTENT_BOMB; k++)
    {
      sum_pieces_known += game->discovered_pieces[k];
    }
  
  int sum_unmoved = 0;
  int sum_unknown_unmoved = 0;

  // count the pieces which has never move
  for(int i=0; i<BOARD_HEIGHT; i++)
    {
      for(int j=0; j<BOARD_WIDTH; j++)
	{
	  if( !game->my_board.data[i][j].has_moved && game->my_board.data[i][j].team == OTHER(num_player) )
	    {
	      sum_unmoved++;
	    }

	  if(!game->my_board.data[i][j].has_moved && game->my_board.data[i][j].content == CONTENT_UNKNOWN )
	    {
	      sum_unknown_unmoved++;
	    }
	}
    }

  int sum_unmovable = game->his_pieces[CONTENT_BOMB] + game->his_pieces[CONTENT_FLAG];
  
  int unknown_pieces = sum_pieces - sum_pieces_known; 

  if( DEBUG_MODE ){  fprintf(stderr, "==> D : sum_unmoved=%d sum_unknown_unmoved=%d sum_pieces_known=%d/%d sum_unmovable=%d sum_pieces=%d\n",sum_unmoved, sum_unknown_unmoved,  sum_pieces_known, sum_pieces, sum_unmovable, sum_pieces);}
    
  
  /* UPDATE PROBABILITIES FOR EACH TILES*/
  for(int i=0; i<BOARD_HEIGHT; i++)
    {
      for(int j=0; j<BOARD_WIDTH; j++)
	{
	  //if this tile is unknown
	  if( game->my_board.data[i][j].team == OTHER(num_player) && game->my_board.data[i][j].content == CONTENT_UNKNOWN )
	    {
	      //for each type of troop
	      for(int k=CONTENT_MARSHAL; k<=CONTENT_BOMB; k++)
		{
		  //if all troops (not flags nor bombs) have moved
		  if( sum_unmovable == sum_unmoved )
		    {
		      //if the current piece has already moved
		      if( game->my_board.data[i][j].has_moved )
			{
			  if( k == CONTENT_BOMB || k == CONTENT_FLAG )
			    {
			      game->my_board.data[i][j].probabilities[k] = 0;
			    }
			  else
			    {
			      game->my_board.data[i][j].probabilities[k] =
				(game->his_pieces[k] - (float)(game->discovered_pieces[k]))/(float)(unknown_pieces - sum_unknown_unmoved);
			    }
			}
		      else
			{
			  // it is obviously a flag or a bomb
			  if( k == CONTENT_BOMB || k == CONTENT_FLAG )
			    {
			      game->my_board.data[i][j].probabilities[k] =
				(float)(game->his_pieces[k] - game->discovered_pieces[k])/(float)sum_unmovable;
			    }
			  else
			    {
			      game->my_board.data[i][j].probabilities[k] = 0;
			    }
			}
		    }
		  else
		    {
		      //if the current piece has already moved
		      if( game->my_board.data[i][j].has_moved )
			{
			  if( k == CONTENT_BOMB || k == CONTENT_FLAG )
			    {
			      game->my_board.data[i][j].probabilities[k] = 0;
			    }
			  else
			    {
			      game->my_board.data[i][j].probabilities[k] =
				(float)(game->his_pieces[k] - game->discovered_pieces[k])/(float)unknown_pieces;
			    }
			}
		      else
			{
			  game->my_board.data[i][j].probabilities[k] =
			    (float)(game->his_pieces[k] - game->discovered_pieces[k])/(float)unknown_pieces;
			}
		    }
		  
		  // a probability reached 1 : we found the value of an unknown piece
		  if( game->my_board.data[i][j].probabilities[k] >= 1
		      && game->my_board.data[i][j].content == CONTENT_UNKNOWN)
		    {
		      something_discovered = 1;
		      game->my_board.data[i][j].content = k;
		      game->discovered_pieces[k] += 1;
		    }
		}
	    }
	}
    }
  
  // we call that function again to recompute if we have discovered
  // the value of a piece ( as in LaTeX )
  if( something_discovered == 1 )
    {
      probabilities_update(num_player, game);
    }
}

// give the sum of the probabilities to meet the target (research) in a direction within a distance
float count_discovered(int num_player, struct game_state *game, int i_start, int j_start, char dir, int dist, int research )
{
  assert( num_player == 0 || num_player == 1 );
  assert( game );
  assert( i_start >= 0);
  assert( j_start >= 0);
  assert( i_start < BOARD_HEIGHT);
  assert( j_start < BOARD_WIDTH);
  
  float sum = 0.0;
  int begin = 0;
  int end = 0;

  // look for the target in the selected direction
  switch( dir )
    {
    case NORTH:
      begin = ( i_start - dist > 0) ? i_start - dist : 0;
      for(int i= begin; i<i_start; i++)
	{
	  for(int j=0; j<BOARD_WIDTH; j++)
	    {
	      if( game->my_board.data[i][j].team == OTHER(num_player) )
		{
		  sum += game->my_board.data[i][j].probabilities[research];
		}
	    }
	}
      break;

    case SOUTH:
      end = ( i_start + dist < BOARD_HEIGHT) ? i_start + dist : BOARD_HEIGHT;
      for(int i= i_start+1; i <= end; i++)
	{
	  for(int j=0; j<BOARD_WIDTH; j++)
	    {
	      if( game->my_board.data[i][j].team == OTHER(num_player) )
		{
		  sum += game->my_board.data[i][j].probabilities[research];
		}
	    }
	}
      break;

    case EAST:
      end = ( j_start + dist < BOARD_WIDTH) ? j_start + dist : BOARD_WIDTH;
      
      for(int i = 0; i< BOARD_HEIGHT; i++)
	{
	  for(int j= j_start + 1 ; j <= end; j++)
	    {
	      if( game->my_board.data[i][j].team == OTHER(num_player) )
		{
		  sum += game->my_board.data[i][j].probabilities[research];
		}
	    }
	}
      break;
      
    case WEST:
      begin = ( j_start - dist > 0) ? j_start - dist : 0;
      
      for(int i = 0; i< BOARD_HEIGHT; i++)
	{
	  for(int j= begin; j < j_start; j++)
	    {
	      if( game->my_board.data[i][j].team == OTHER(num_player) )
		{
		  sum += game->my_board.data[i][j].probabilities[research];
		}
	    }
	}
      break;

    }

  return sum;
}

// evaluate if the destination should be avoided 
int avoid(int num_player, struct game_state* game, int *avoids, float *threshold, int *danger_valuation, size_t nb_avoids, int i, int j, int new_i,int new_j)
{
  assert( avoid );
  assert( threshold );
  assert( game );
  assert( num_player == 0 || num_player == 1 );

  // when only 20 soldiers remains (end game) 
  if( game->pieces_count <= 20 )
    {
      // we don't avoid anymore
      return 0;
    }
  
  /* AVOID */
  // for each neighbor
  for(int i_close = new_i - 1; i_close <= new_i + 1; i_close++)
    {
      for(int j_close = new_j - 1; j_close <= new_j + 1; j_close++)
	{
	  if( exists(i_close, j_close)//test if the tile exists in the board
	      && !(i_close == i && j_close == j)//test if it isn't myself
	      && abs(new_i - i_close) != abs(new_j - j_close)//test if it isn't a diagonales
	      && game->my_board.data[i_close][j_close].team == OTHER(num_player) )//test if it is the opponent's team
	    {
	      //for each dangerous pieces
	      for(int k = 0; k<nb_avoids; k++)
		{
		  //if the probability to encounter that piece is too important
		  if( game->my_board.data[i_close][j_close].probabilities[ avoids[k] ] <= threshold[k] )
		    {
		      return EVALUATE_NEUTRAL + danger_valuation[k];
		    }
		}
	    }
	}
    }
  
  return 0;
}

// evaluate if we are moving in the direction of a hunted piece
int hunt(int num_player , struct game_state *game, int i, int j, char dir,
	 int dist, int* targets, size_t nb_targets, int hunt_radius, float threshold)
{
  assert(num_player == 0 || num_player == 1);
  assert( game );
  assert( targets );
  
  int current_target = 0;
  float proba = 0.0;

  // if a hunted target no longer exists, we hunt the next one
  while( game->his_pieces[ targets[current_target] ] == 0 && current_target < nb_targets)
    {
      current_target++;
    }
      
  proba = count_discovered(num_player, game, i, j, dir, hunt_radius, targets[current_target] );

  // the probability to encounter the target in that direction
  // is high enough
  if( proba >= threshold )
    {
      return EVALUATE_NEUTRAL + (int)(proba * REWARD_HUNT/game->his_pieces[targets[current_target]]);
    }
  else
    // it is better to go forward than backward
    {
      if( ( dir == EAST && num_player == 0 ) || ( dir == WEST && num_player == 1 ) )
	{
	  return EVALUATE_NEUTRAL + REWARD_WALK;
	}
      
      return EVALUATE_NEUTRAL + 2*REWARD_WALK;
    }
}
