#ifndef STRATEGO_H
#define STRATEGO_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <sys/time.h>

// X is a player, OTHER(X) return the other player  (1 gives 0 and 0 gives -1)
#define OTHER(X) ((X+1)%2)
// X is a coordinate ( i or j ) of a 10x10 board. SYM(X) return the opposite of the coordinate on the board;
#define SYM(X) ((int)(4.5 + (num_player*2 - 1) * (4.5 - X ) ))
// cf SYM(X) return the opposite for the other player
#define SYM_OTHER(X) ((int)(4.5 + (OTHER(num_player)*2 - 1) * (4.5 - X )))

#define BUFSIZE 256
#define BOARD_WIDTH 10
#define BOARD_HEIGHT 10
#define PIECE_COUNT 15
#define STEP_TIME_MAX 450000 //microseconds
#define EVALUATE_NEUTRAL 5000
#define DEBUG_MODE 1

enum STYLE
  {
    STYLE_NONE = 0,
    STYLE_UNDERLINE = 4
  };

enum COLOR
  {
    COLOR_BLACK=0,
    COLOR_RED=1,
    COLOR_GREEN=2,
    COLOR_YELLOW=3,
    COLOR_BLUE=4,
    COLOR_MAGENTA=5,
    COLOR_CYAN=6,
    COLOR_WHITE=7
  };

//number of pieces required for one player
static const int quantity_rule[PIECE_COUNT] = 
  {
    12, //NONE
    1, //MARSHAL
    1, //GENERAL
    2, //COLONEL
    3, //COMMANDER
    4, //CAPTAIN
    4, //LIEUTENANT
    4, //SERGENT
    5, //MINESWEEPER
    8, //SCOUT
    1, //SPY
    1, //FLAG
    6, //BOMB
    40, //UNKNOWN
    8  //WATER
  };


static const char display_rule[PIECE_COUNT] = 
  {
    ' ', //NONE
    '0', //MARSHAL
    '9', //GENERAL
    '8', //COLONEL
    '7', //COMMANDER
    '6', //CAPTAIN
    '5', //LIEUTENANT
    '4', //SERGENT
    '3', //MINESWEEPER
    '2', //SCOUT
    'S', //SPY
    'F', //FLAG
    'B', //BOMB
    '?', //UNKNOWN
    '~'  //WATER
  };


enum content
  {
    CONTENT_NONE = 0,
    CONTENT_MARSHAL = 1,
    CONTENT_GENERAL = 2,
    CONTENT_COLONEL = 3,
    CONTENT_COMMANDER = 4,
    CONTENT_CAPTAIN = 5,
    CONTENT_LIEUTENANT = 6,
    CONTENT_SERGENT = 7,
    CONTENT_MINESWEEPER = 8,
    CONTENT_SCOUT = 9,
    CONTENT_SPY = 10,
    CONTENT_FLAG = 11,
    CONTENT_BOMB = 12,
    CONTENT_UNKNOWN = 13,
    CONTENT_WATER = 14
  };

enum reward
  {
    REWARD_FLAG = 2000,
    REWARD_MARSHAL =  1100,
    REWARD_GENERAL =  1000,
    REWARD_SPY =  900,
    REWARD_COLONEL =  800,
    REWARD_MINESWEEPER =  700,
    REWARD_BOMB =  650,
    REWARD_COMMANDER =  600,
    REWARD_CAPTAIN =  500,
    REWARD_LIEUTENANT =  400,
    REWARD_SCOUT =  200,
    REWARD_SERGENT =  150
  };


struct cell
{
  int content;// value of the pieces (cf enum content)
  int team;// player 0 (0)   player 1 (1)   neutral (-1)
  float probabilities[PIECE_COUNT];
  int has_moved;// never moved (0) or already moved (1)
};

enum direction
  {
    NORTH = 'N',
    SOUTH = 'S',
    EAST = 'E',
    WEST = 'W'
  };

struct board
{
  struct cell data[BOARD_HEIGHT][BOARD_WIDTH];
};

/* INIT */
void board_init_empty(struct board *self);

/* DISPLAY */
void board_display(struct board const* self);

//color
void print_color(char *str, int fg, int bg, int option);
void board_color_display(struct board const* self);

/* UTILITIES */
void reset_tile(struct board* board,int i,int j);
int random(int a, int b);
suseconds_t get_microseconds();
int piece_cmp(int att, int def);
int place_one_content(int num_player, struct board* board, int content, int i, int j);
int check_place(int num_player, struct board* board);
int exists(int i, int j);
int pos_is_valid(struct board const* self, int i, int j);
//movements
int move_is_valid(int player, struct board const* self,  int i, int j, char dir, int dist);
int move_one_content(int num_player, struct board* board, int i, int j, char dir, int dist);
void get_destination(int old_i, int old_j, int* i,int* j, char dir, int dist);


/* INPUTS/OUTPUTS */

// input
int read_one_line(char* str);
int read_one_line_in(char* str, int fd);

// output
int write_one_line(char* what, int where);
int write_out(char* what);
int write_err(char* what);

#endif
