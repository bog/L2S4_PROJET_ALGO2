#define _POSIX_SOURCE 
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "stratego-ossete_gombe-dumontet.h"
#include "referee-ossete_gombe-dumontet.h"

int victory = -1;// nobody won
pid_t pid_player0 = 0;
pid_t pid_player1 = 0;

// wait a dead child
void infanticide()
{
  //printf("On ne peut pas les tuer, ils sont déjà mort !\n");//DEBUG
  pid_t pid_terminated = wait(NULL);

  // if the game isn't finished, pid_terminated has lost the game
  if( victory == -1 )
    {
      int player_terminated = (pid_player0 == pid_terminated) ? 0 : 1;
      victory = OTHER(player_terminated);
      printf("--> Player %d has terminated...\n", player_terminated );
      printf("--> Player %d failed : player %d WON\n", OTHER(victory), victory );

      exit(EXIT_SUCCESS);
    }
}

int main(int argc, char** argv)
{
  /* TITLE */
  write(1, "\n",2);
  print_color("             ", COLOR_RED, COLOR_WHITE, STYLE_UNDERLINE);
  print_color(" STRATEGO ", COLOR_RED, COLOR_WHITE, STYLE_UNDERLINE);
  print_color("             ", COLOR_RED, COLOR_WHITE, STYLE_UNDERLINE);
  write(1, "\n",2);

  write(1, "\n",2);
  print_color(" by  ", COLOR_RED, COLOR_GREEN, STYLE_NONE);
  print_color(" Bérenger OSSETE GOMBE ", COLOR_GREEN, COLOR_BLACK, STYLE_NONE);
  write(1, "\n",2);
  print_color(" and ", COLOR_RED, COLOR_GREEN, STYLE_NONE);
  print_color(" Sylvain  DUMONTET     ", COLOR_GREEN, COLOR_BLACK, STYLE_NONE);
  
  write(1, "\n",2);
  write(1, "\n",2);
  
  sleep(1);

  /* INIT */
  if( argc != 3 )
    {
      fprintf(stderr, "E: Need two players !\n");
      return EXIT_FAILURE;
    }

  srand(time(NULL));

  // players' names
  char* player0 = argv[1];
  char* player1 = argv[2];

  mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

  // players' log files
  int player0_log = creat("player0.log", mode);
  int player1_log = creat("player1.log", mode);

  /* PREPARE PIPES, FORK AND EXEC FOR PLAYER 0  */
  int p0_in[2];
  pipe(p0_in);
  
  int p0_out[2];
  pipe(p0_out);
  
  if( (pid_player0 = fork()) == 0 )
    {
      close(p0_in[1]);
      dup2(p0_in[0], 0);
      close(p0_in[0]);

      close(p0_out[0]);
      dup2(p0_out[1], 1);
      close(p0_out[1]);

      close(player1_log);
      dup2( player0_log, 2);
      close(player0_log);
      
      execl(player0, player0, NULL);
      exit(EXIT_FAILURE);
    }

  close(p0_in[0]);
  close(p0_out[1]);

  /* PREPARE PIPES, FORK AND EXEC FOR PLAYER 0  */
  int p1_in[2];
  pipe(p1_in);
  
  int p1_out[2];
  pipe(p1_out);
  
  if( (pid_player1 = fork()) == 0 )
    {
      close(p1_in[1]);
      dup2(p1_in[0], 0);
      close(p1_in[0]);

      close(p1_out[0]);
      dup2(p1_out[1], 1);
      close(p1_out[1]);

      close(player0_log);
      dup2( player1_log, 2);
      close(player1_log);
      
      execl(player1, player1, NULL);
      exit(EXIT_FAILURE);
    }
  
  close(p1_in[0]);
  close(p1_out[1]);
  
  /* REFEREE */
  // signal handling (SIGCHILD)
  struct sigaction sa_old;
  struct sigaction sa;
  sigemptyset(&sa.sa_mask);
  sa.sa_handler = infanticide;
  sa.sa_flags = 0;

  sigaction(SIGCHLD, &sa, &sa_old);

  // init referee's game state
  int p0_number = 0;
  int p1_number = 1;
  struct board myboard;
  board_init_empty(&myboard);

  int p0_output = p0_out[0];
  int p0_input = p0_in[1];

  int p1_output = p1_out[0];
  int p1_input = p1_in[1];
  
  /* COMMUNICATE NUMBER */
  char p0_num_string[BUFSIZE];
  char p1_num_string[BUFSIZE];
  
  sprintf(p0_num_string, "%d\n", p0_number);
  sprintf(p1_num_string, "%d\n", p1_number);
  
  write_one_line(p0_num_string, p0_input);
  write_one_line(p1_num_string, p1_input);

  /* CHECK STARTING PLACEMENT FOR BOTH PLAYERS */
  if( !get_pieces_from_one_player(0,&myboard, p0_output) )
    {
      write_one_line("KO\n", p0_input);
      //fprintf(stderr, "PLAYER 0 KO\n");//DEBUG
    }
  else
    {
      write_one_line("OK\n", p0_input);
      //fprintf(stderr, "PLAYER 0 OK\n");//DEBUG
    }

  if( !get_pieces_from_one_player(1,&myboard, p1_output) )
    {
      write_one_line("KO\n", p1_input);
      //fprintf(stderr, "PLAYER 1 KO\n");//DEBUG
    }
  else
    {
      write_one_line("OK\n", p1_input);
      //fprintf(stderr, "PLAYER 1 OK\n");//DEBUG
    }
  

  /* GAME LOOP */
  int turn = 1;
  
  while( victory == -1 )
    {
      // print turn and player0's name
      print_color("  ", COLOR_GREEN, COLOR_GREEN, STYLE_NONE);
      char tmp[BUFSIZE];
      sprintf(tmp," TURN %d : %s ",turn, player0);
      write(1, tmp, strlen(tmp));
      write(1,"\n",2);

      board_color_display(&myboard);

      if( DEBUG_MODE_REFEREE ){fprintf(stderr, "D : player 0 !!!\n");}//DEBUG
      
      /* PLAYER 0's TURN*/
      if(!player_turn(0, &myboard, p0_input, p0_output, p1_input, &victory) )
	{
	  continue;
	}
      
      // print turn and player1's name
      print_color("  ", COLOR_RED, COLOR_RED, STYLE_NONE);
      sprintf(tmp," TURN %d : %s ",turn, player1);
      write(1, tmp, strlen(tmp));
      write(1,"\n",2);
      board_color_display(&myboard);

      
      if( DEBUG_MODE_REFEREE ){fprintf(stderr, "D : player 1 !!!\n");}//DEBUG

      /* PLAYER 1's TURN */
      if(!player_turn(1, &myboard, p1_input, p1_output, p0_input, &victory) )
	{
	  continue;
	}

      turn++;
    }

  /* DISPLAY RESULT */
  printf("TURN %d\n",turn);
  board_color_display(&myboard);

  char tmp[BUFSIZE];
  sprintf(tmp, "Player %s FAILED ", (victory!=0) ? player0 : player1 );
  print_color(tmp, (victory!=0) ? COLOR_GREEN : COLOR_RED, COLOR_BLACK, STYLE_UNDERLINE);
  write(1,"\n",2);
  
  sprintf(tmp, "Player %s WON ", (victory==0) ? player0 : player1 );
  print_color(tmp, (victory==0) ? COLOR_GREEN : COLOR_RED, COLOR_BLACK, STYLE_UNDERLINE);
  write(1,"\n",2);
  
  /* CLOSE FILE DESCRIPTOR */
  close(p0_in[1]);
  close(p0_out[0]);
  close(p1_in[1]);
  close(p1_out[0]);

  close(player0_log);
  close(player1_log);
  
  /* SIGTERM CHILDREN */
  kill(pid_player0, SIGTERM);
  kill(pid_player1, SIGTERM);
  
  sigaction(SIGCHLD, &sa_old, NULL);
  
  return 0;
}


// get the initial placement from num_player
int get_pieces_from_one_player(int num_player, struct board* board, int p)
{
  assert( num_player == 0 || num_player == 1);
  assert( board );
  
  struct board player_board;
  board_init_empty(&player_board);
  
  for(int i=0; i<10; i++)
    {
      for(int j=0; j<4; j++)
	{
	  char value_str[BUFSIZE];
	  read_one_line_in(value_str,p);
	  int value = atoi(value_str);
	  player_board.data[SYM(i)][SYM(j)].content = value;
	  player_board.data[SYM(i)][SYM(j)].team = num_player;

	  player_board.data[SYM_OTHER(i)][SYM_OTHER(j)].content = CONTENT_UNKNOWN;
	  player_board.data[SYM_OTHER(i)][SYM_OTHER(j)].team = OTHER(num_player);

	  board->data[i][j + 6*num_player].content = value;
	  board->data[i][j + 6*num_player].team = num_player;
	}
    }
  
  //board_display(&player_board);//DEBUG
  
  if( !check_place(num_player, &player_board) )
    {
      return 0;
    }

  return 1;
}

// get the next step from one player and fill i, j, dir and dist
int get_step(int* i, int *j, char* dir, int* dist, int p)
{
  assert( i );
  assert( j );
  assert( dir );
  assert( dist );
  
  char tmp;
  char buffer[BUFSIZE];

  size_t count = 0;
  size_t count_return = 0;
  suseconds_t begin = get_microseconds();

  while(get_microseconds() - begin <= TURN_TIME_MAX
	 && count_return < 3
	 && read(p, &tmp, 1) > 0 )
    {
      buffer[count] = tmp;
      count++;
      
      if(tmp == '\n')
	{
	  count_return++;

	}
    }

  // the player has been too slow
  if(get_microseconds() - begin > TURN_TIME_MAX)
    {
      printf("Elapsed time %zd (> %ds !)\n", get_microseconds() - begin, TURN_TIME_MAX);
      return 0;
    }
  
  buffer[count+1]='\0';

  *i = buffer[0] - 'A';
  *j = buffer[1] - '0';

  *dir = buffer[3];
  *dist = buffer[5] - '0';

  // wait during the remaining time
  while( get_microseconds() - begin < TURN_TIME_DISPLAY )
    {
    }
  
  return 1;
}

// get the player step, analyse it and update the board
int player_turn(int num_player, struct board *board, int pla_input, int pla_output, int adv_input, int* victory)
{
  int i = 0;
  int j = 0;
  char dir = ' ';
  int dist = 0;

  int new_i = 0;
  int new_j = 0;

  // get the player step
  if( !get_step(&i, &j, &dir, &dist, pla_output) )
    {
      *victory = OTHER(num_player);
      return 0;
    }

  get_destination(i, j, &new_i, &new_j, dir, dist);

  if( DEBUG_MODE_REFEREE ){fprintf(stderr, "STEP(%d) : %c %d %c %d\n",num_player, i + 'A', j, dir, dist);}//DEBUG

  // analyse step
  if( !move_is_valid(num_player, board, i, j, dir, dist) )
    {
      write_one_line("E\n", pla_input);
      write_one_line("F\n", adv_input);
      *victory = OTHER(num_player);
      return 0;
    }
  else
    {
      // send step to other player
      char step[BUFSIZE];
      snprintf(step, 8, "%c%d\n%c\n%d\n", i + 'A', j, dir, dist);
      write_one_line(step, adv_input);
      
      if( DEBUG_MODE_REFEREE ){fprintf(stderr, "D(%d) : piece_value = %d dest = %d %d ( value=%d)\n",num_player, board->data[i][j].content, new_i, new_j,board->data[new_i][new_j].content);}//DEBUG
      
      // battle
      if( board->data[new_i][new_j].team == OTHER(num_player) )
	{
	  if( DEBUG_MODE_REFEREE ){fprintf(stderr, "D(%d): FIGHT !! \n",num_player);}//DEBUG
	  
	  char battle_step[BUFSIZE];
	  int additional_char = (board->data[i][j].content >= 10) +(board->data[new_i][new_j].content >= 10);
	  snprintf(battle_step, 7 + additional_char, "B\n%d\n%d\n", board->data[i][j].content, board->data[new_i][new_j].content);

	  write_one_line(battle_step, pla_input);
	  write_one_line(battle_step, adv_input);
	      
	  // solve battle
	  int value_attack = board->data[i][j].content;
	  int value_defend = board->data[new_i][new_j].content;
	  
	  if( DEBUG_MODE_REFEREE ){fprintf(stderr, "FIGHT(%d):  attack : %d vs defend : %d \n",num_player, value_attack, value_defend);}//DEBUG
	  
	  int battle_result = piece_cmp(value_attack, value_defend);

	  if(value_defend == CONTENT_FLAG)
	    {
	      reset_tile(board, new_i, new_j);
	      move_one_content(num_player , board, i, j, dir, dist);
	      
	      *victory = num_player;
	      return 0;
	    }
	  else if( battle_result > 0 )
	    {
	      reset_tile(board, new_i, new_j);
		      
	      move_one_content(num_player , board, i, j, dir, dist);

	    }
	  else if( battle_result < 0 )
	    {
	      reset_tile(board, i, j);
	    }
	  else
	    {
	      reset_tile(board, i, j);
	      reset_tile(board, new_i, new_j);
	    }
	}
      // update the board
      else
	{
	  write_one_line("N\n", pla_input);
	  write_one_line("N\n", adv_input);
	      
	  move_one_content(num_player, board, i, j, dir, dist);
	}
    }

  return 1;
}
