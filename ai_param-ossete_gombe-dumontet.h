#ifndef AI_PARAM_H
#define AI_PARAM_H
#include "stratego-ossete_gombe-dumontet.h"

#define REWARD_HUNT 100
#define REWARD_WALK -10
#define STEP_RANDOM_THRESHOLD 0.5

/*
STRATEGY : 

All the soldiers have three differents behaviors :
- avoid -> look at the neighbor of the destination in order to avoid some potentials dangers
- hunt -> prefere the direction where there are choosen targets
- fight -> look at the result of a fight agains each type of soldier in the neighbors

When only 20 pieces remains, avoid behavior is disabled, and
when only 10 enemies pieces remains, the weight of minesweepers'actions is over-evaluated.

All the following data allow us to balance the behaviors.

*/

/* ************************************************** MARSHAL ************************************************** */
static int MARSHAL_REWARDS[PIECE_COUNT] =
  {
    -1,
    -500,
    1200,
    1000,
    800,
    500,
    300,
    150,
    250,
    100,
    800,
    2000,
    -500,
    200,
    -1
    
  };

#define MARSHAL_NB_AVOID 2

static int MARSHAL_AVOID[MARSHAL_NB_AVOID] =
  {
    CONTENT_MARSHAL,
    CONTENT_SPY
  };

static float MARSHAL_AVOID_THRESHOLD[MARSHAL_NB_AVOID] =
  {
    0.2,
    0.1
  };

static int MARSHAL_AVOID_DANGER[MARSHAL_NB_AVOID] =
  {
    -500,
    -1000
  };

#define MARSHAL_HUNT_NB_TARGETS 11

static int MARSHAL_HUNT_TARGETS[MARSHAL_HUNT_NB_TARGETS] =
  {
    CONTENT_GENERAL,
    CONTENT_COLONEL,
    CONTENT_COMMANDER,
    CONTENT_CAPTAIN,
    CONTENT_LIEUTENANT,
    CONTENT_SERGENT,
    CONTENT_MINESWEEPER,
    CONTENT_SCOUT,
    CONTENT_SPY,
    CONTENT_MARSHAL,
    CONTENT_FLAG
  };

#define MARSHAL_HUNT_THRESHOLD 0.5

#define MARSHAL_HUNT_RADIUS 10


/* ************************************************** GENERAL ************************************************** */
static int GENERAL_REWARDS[PIECE_COUNT] =
  {
    -1,
    -500,
    -100,
    1000,
    800,
    500,
    300,
    150,
    250,
    100,
    800,
    2000,
    -500,
    200,
    -1
    
  };

#define GENERAL_NB_AVOID 2

static int GENERAL_AVOID[GENERAL_NB_AVOID] =
  {
    CONTENT_MARSHAL,
    CONTENT_GENERAL
  };

static float GENERAL_AVOID_THRESHOLD[GENERAL_NB_AVOID] =
  {
    0.1,
    0.2
  };

static int GENERAL_AVOID_DANGER[GENERAL_NB_AVOID] =
  {
    -500,
    -100
  };

#define GENERAL_HUNT_NB_TARGETS 10

static int GENERAL_HUNT_TARGETS[GENERAL_HUNT_NB_TARGETS] =
  {
    CONTENT_COLONEL,
    CONTENT_COMMANDER,
    CONTENT_CAPTAIN,
    CONTENT_LIEUTENANT,
    CONTENT_SERGENT,
    CONTENT_MINESWEEPER,
    CONTENT_SCOUT,
    CONTENT_SPY,
    CONTENT_GENERAL,
    CONTENT_FLAG
  };

#define GENERAL_HUNT_THRESHOLD 0.5

#define GENERAL_HUNT_RADIUS 10

/* ************************************************** COLONEL ************************************************** */
static int COLONEL_REWARDS[PIECE_COUNT] =
  {
    -1,
    -500,
    -500,
    -100,
    800,
    500,
    300,
    150,
    250,
    100,
    800,
    2000,
    -500,
    150,
    -1
    
  };

#define COLONEL_NB_AVOID 2

static int COLONEL_AVOID[COLONEL_NB_AVOID] =
  {
    CONTENT_MARSHAL,
    CONTENT_GENERAL
  };

static float COLONEL_AVOID_THRESHOLD[COLONEL_NB_AVOID] =
  {
    0.15,
    0.15
  };

static int COLONEL_AVOID_DANGER[COLONEL_NB_AVOID] =
  {
    -500,
    -500
  };

#define COLONEL_HUNT_NB_TARGETS 9

static int COLONEL_HUNT_TARGETS[COLONEL_HUNT_NB_TARGETS] =
  {
    CONTENT_COMMANDER,
    CONTENT_CAPTAIN,
    CONTENT_LIEUTENANT,
    CONTENT_SERGENT,
    CONTENT_MINESWEEPER,
    CONTENT_SCOUT,
    CONTENT_SPY,
    CONTENT_COLONEL,
    CONTENT_FLAG
  };

#define COLONEL_HUNT_THRESHOLD 0.5

#define COLONEL_HUNT_RADIUS 10



/* ************************************************** COMMANDER ************************************************** */
static int COMMANDER_REWARDS[PIECE_COUNT] =
  {
    -1,
    -400,
    -400,
    -400,
    -100,
    500,
    300,
    150,
    250,
    100,
    800,
    2000,
    -500,
    150,
    -1
    
  };

#define COMMANDER_NB_AVOID 3

static int COMMANDER_AVOID[COMMANDER_NB_AVOID] =
  {
    CONTENT_MARSHAL,
    CONTENT_GENERAL,
    CONTENT_COLONEL
  };

static float COMMANDER_AVOID_THRESHOLD[COMMANDER_NB_AVOID] =
  {
    0.5,
    0.2,
    0.1
  };

static int COMMANDER_AVOID_DANGER[COMMANDER_NB_AVOID] =
  {
    -400,
    -400
  };

#define COMMANDER_HUNT_NB_TARGETS 8

static int COMMANDER_HUNT_TARGETS[COMMANDER_HUNT_NB_TARGETS] =
  {
    CONTENT_CAPTAIN,
    CONTENT_MINESWEEPER,
    CONTENT_LIEUTENANT,
    CONTENT_SERGENT,
    CONTENT_SCOUT,
    CONTENT_SPY,
    CONTENT_COMMANDER,
    CONTENT_FLAG

  };

#define COMMANDER_HUNT_THRESHOLD 0.5

#define COMMANDER_HUNT_RADIUS 10


/* ************************************************** CAPTAIN ************************************************** */
static int CAPTAIN_REWARDS[PIECE_COUNT] =
  {
    -1,
    -400,
    -400,
    -400,
    -400,
    -100,
    300,
    150,
    250,
    100,
    800,
    2000,
    -500,
    200,
    -1
    
  };

#define CAPTAIN_NB_AVOID 4

static int CAPTAIN_AVOID[CAPTAIN_NB_AVOID] =
  {
    CONTENT_MARSHAL,
    CONTENT_GENERAL,
    CONTENT_COLONEL,
    CONTENT_COMMANDER
  };

static float CAPTAIN_AVOID_THRESHOLD[CAPTAIN_NB_AVOID] =
  {
    0.5,
    0.2,
    0.15,
    0.1
  };

static int CAPTAIN_AVOID_DANGER[CAPTAIN_NB_AVOID] =
  {
    -400,
    -400
  };

#define CAPTAIN_HUNT_NB_TARGETS 7

static int CAPTAIN_HUNT_TARGETS[CAPTAIN_HUNT_NB_TARGETS] =
  {
    CONTENT_MINESWEEPER,
    CONTENT_LIEUTENANT,
    CONTENT_SERGENT,
    CONTENT_SCOUT,
    CONTENT_SPY,
    CONTENT_CAPTAIN,
    CONTENT_FLAG

  };

#define CAPTAIN_HUNT_THRESHOLD 0.5

#define CAPTAIN_HUNT_RADIUS 10


/* ************************************************** LIEUTENANT ************************************************** */
static int LIEUTENANT_REWARDS[PIECE_COUNT] =
  {
    -1,
    -300,
    -300,
    -300,
    -300,
    -300,
    -50,
    150,
    250,
    100,
    800,
    2000,
    -500,
    200,
    -1
    
  };

#define LIEUTENANT_NB_AVOID 5

static int LIEUTENANT_AVOID[LIEUTENANT_NB_AVOID] =
  {
    CONTENT_MARSHAL,
    CONTENT_GENERAL,
    CONTENT_COLONEL,
    CONTENT_COMMANDER,
    CONTENT_CAPTAIN
  };

static float LIEUTENANT_AVOID_THRESHOLD[LIEUTENANT_NB_AVOID] =
  {
    1.0,
    0.9,
    0.8,
    0.7,
    0.6
  };

static int LIEUTENANT_AVOID_DANGER[LIEUTENANT_NB_AVOID] =
  {
    -300,
    -300
  };

#define LIEUTENANT_HUNT_NB_TARGETS 6

static int LIEUTENANT_HUNT_TARGETS[LIEUTENANT_HUNT_NB_TARGETS] =
  {
    CONTENT_SPY,
    CONTENT_MINESWEEPER,
    CONTENT_SERGENT,
    CONTENT_SCOUT,
    CONTENT_LIEUTENANT,
    CONTENT_FLAG
  };

#define LIEUTENANT_HUNT_THRESHOLD 0.5

#define LIEUTENANT_HUNT_RADIUS 10


/* ************************************************** SERGENT ************************************************** */
static int SERGENT_REWARDS[PIECE_COUNT] =
  {
    -1,
    -300,
    -300,
    -300,
    -300,
    -300,
    -300,
    -50,
    250,
    100,
    800,
    2000,
    -500,
    250,
    -1
    
  };

#define SERGENT_NB_AVOID 6

static int SERGENT_AVOID[SERGENT_NB_AVOID] =
  {
    CONTENT_MARSHAL,
    CONTENT_GENERAL,
    CONTENT_COLONEL,
    CONTENT_COMMANDER,
    CONTENT_CAPTAIN,
    CONTENT_LIEUTENANT
  };

static float SERGENT_AVOID_THRESHOLD[SERGENT_NB_AVOID] =
  {
    1.0,
    0.9,
    0.8,
    0.7,
    0.6,
    0.5
  };

static int SERGENT_AVOID_DANGER[SERGENT_NB_AVOID] =
  {
    -300
  };

#define SERGENT_HUNT_NB_TARGETS 5

static int SERGENT_HUNT_TARGETS[SERGENT_HUNT_NB_TARGETS] =
  {
    CONTENT_SPY,
    CONTENT_MINESWEEPER,
    CONTENT_FLAG,
    CONTENT_SCOUT,
    CONTENT_SERGENT
  };

#define SERGENT_HUNT_THRESHOLD 0.5

#define SERGENT_HUNT_RADIUS 10


/* ************************************************** MINESWEEPER ************************************************** */
static int MINESWEEPER_REWARDS[PIECE_COUNT] =
  {
    -1,
    -400,
    -400,
    -400,
    -400,
    -400,
    -400,
    -400,
    -150,
    100,
    800,
    2000,
    1000,
    -50,
    -1
    
  };

#define MINESWEEPER_NB_AVOID 7

static int MINESWEEPER_AVOID[MINESWEEPER_NB_AVOID] =
  {
    CONTENT_MARSHAL,
    CONTENT_GENERAL,
    CONTENT_COLONEL,
    CONTENT_COMMANDER,
    CONTENT_CAPTAIN,
    CONTENT_LIEUTENANT,
    CONTENT_SERGENT
  };

static float MINESWEEPER_AVOID_THRESHOLD[MINESWEEPER_NB_AVOID] =
  {
    0.2,
    0.2,
    0.2,
    0.1,
    0.1,
    0.1,
    0.1//LAST THING DONE
  };

static int MINESWEEPER_AVOID_DANGER[MINESWEEPER_NB_AVOID] =
  {
    -300,
    -300,
    -300,
    -300,
    -300,
    -300,
    -300
  };

#define MINESWEEPER_HUNT_NB_TARGETS 4

static int MINESWEEPER_HUNT_TARGETS[MINESWEEPER_HUNT_NB_TARGETS] =
  {
    CONTENT_BOMB,
    CONTENT_FLAG,
    CONTENT_MINESWEEPER,
    CONTENT_SCOUT
  };

#define MINESWEEPER_HUNT_THRESHOLD 0.1

#define MINESWEEPER_HUNT_RADIUS 10


/* ************************************************** SCOUT ************************************************** */
static int SCOUT_REWARDS[PIECE_COUNT] =
  {
    -1,
    -100,
    -100,
    -100,
    -100,
    -100,
    -100,
    -100,
    -100,
    -30,
    800,
    2000,
    -500,
    500,
    -1
    
  };

#define SCOUT_NB_AVOID 8

static int SCOUT_AVOID[SCOUT_NB_AVOID] =
  {
    CONTENT_MARSHAL,
    CONTENT_GENERAL,
    CONTENT_COLONEL,
    CONTENT_COMMANDER,
    CONTENT_CAPTAIN,
    CONTENT_LIEUTENANT,
    CONTENT_SERGENT,
    CONTENT_MINESWEEPER
  };

static float SCOUT_AVOID_THRESHOLD[SCOUT_NB_AVOID] =
  {
    1.0,
    1.0,
    0.9,
    0.9,
    0.9,
    0.9,
    0.9,
    0.9
  };

static int SCOUT_AVOID_DANGER[SCOUT_NB_AVOID] =
  {
    -100
  };

#define SCOUT_HUNT_NB_TARGETS 3

static int SCOUT_HUNT_TARGETS[SCOUT_HUNT_NB_TARGETS] =
  {
    CONTENT_SPY,
    CONTENT_FLAG,
    CONTENT_SCOUT
  };

#define SCOUT_HUNT_THRESHOLD 0.5

#define SCOUT_HUNT_RADIUS 10


/* ************************************************** SPY ************************************************** */
static int SPY_REWARDS[PIECE_COUNT] =
  {
    -1,
    1000,
    -500,
    -500,
    -500,
    -500,
    -500,
    -500,
    -500,
    -500,
    -400,
    2000,
    -500,
    -200,
    -1
    
  };

#define SPY_NB_AVOID 8

static int SPY_AVOID[SPY_NB_AVOID] =
  {
    CONTENT_GENERAL,
    CONTENT_COLONEL,
    CONTENT_COMMANDER,
    CONTENT_CAPTAIN,
    CONTENT_LIEUTENANT,
    CONTENT_SERGENT,
    CONTENT_MINESWEEPER,
    CONTENT_SCOUT
  };

static float SPY_AVOID_THRESHOLD[SPY_NB_AVOID] =
  {
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0
  };

static int SPY_AVOID_DANGER[SPY_NB_AVOID] =
  {
    -500,
    -500,
    -500,
    -500,
    -500,
    -500,
    -500,
    -500
  };

#define SPY_HUNT_NB_TARGETS 3

static int SPY_HUNT_TARGETS[SPY_HUNT_NB_TARGETS] =
  {
    CONTENT_MARSHAL,
    CONTENT_SPY,
    CONTENT_FLAG
  };

#define SPY_HUNT_THRESHOLD 0.5

#define SPY_HUNT_RADIUS 10

#endif
