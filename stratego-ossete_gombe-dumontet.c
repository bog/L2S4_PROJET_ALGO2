#include "stratego-ossete_gombe-dumontet.h"

/* INIT */

// init the board with water and empty tiles
void board_init_empty(struct board *self)
{
  assert(self);
  
  for(int i=0; i<BOARD_HEIGHT; i++)
    {
      for(int j=0; j<BOARD_WIDTH; j++)
	{
	  self->data[i][j].content = CONTENT_NONE;
	  self->data[i][j].team = -1;
	  self->data[i][j].has_moved = 0;
	  
	  for(int k=0; k<PIECE_COUNT; k++)
	    {
	      self->data[i][j].probabilities[k] = 0.0;
	    }
	}
    }

  //lake 1
  self->data[2][4].content = CONTENT_WATER;
  self->data[2][5].content = CONTENT_WATER;
  self->data[3][4].content = CONTENT_WATER;
  self->data[3][5].content = CONTENT_WATER;
		  
  //lake 2	  	     
  self->data[6][4].content = CONTENT_WATER;
  self->data[6][5].content = CONTENT_WATER;
  self->data[7][4].content = CONTENT_WATER;
  self->data[7][5].content = CONTENT_WATER; 
}

/* DISPLAY */

//display the board without colors (unused in normal mode but usefull for debug)
//for classical display cf board_color_display
void board_display(struct board const* self)
{
  assert(self);

  for(int i=-1; i<BOARD_HEIGHT+1; i++)
    {
      for(int j=-1; j<BOARD_WIDTH+1; j++)
	{
	  // border
	  if((i == -1 || i == BOARD_HEIGHT) && (j == -1 || j == BOARD_WIDTH))
	    {
	      printf("###");
	    }
	  if( (i == -1 || i == BOARD_HEIGHT) && !(j == -1 || j == BOARD_WIDTH) )
	    {
	      printf("#%d#", j);
	    }
	  if( (j == -1 || j == BOARD_WIDTH) && !(i == -1 || i == BOARD_HEIGHT) )
	    {
	      printf("#%c#", i+'A');
	    }
	  
	  // board
	  if( j > -1 && j < BOARD_WIDTH && i > -1 && i < BOARD_HEIGHT )
	    {
	      if( self->data[i][j].team == 0 ) printf("+");//player 0
	      else if(self->data[i][j].team == 1) printf("-");//player 1
	      else printf(" ");//neutral

	      printf("%c ", display_rule[self->data[i][j].content] );
	    }
	}
      printf("\n\n");
    }
}

//print the string str with a color fg (foreground) and the background color bg using an option
// cf enum color and enum style
void print_color(char *str, int fg, int bg, int option)
{
  assert(str);
  assert(fg >= COLOR_BLACK);
  assert(fg <= COLOR_WHITE);
  assert(bg >= COLOR_BLACK);
  assert(bg <= COLOR_WHITE);
  assert(option >= STYLE_NONE);
  assert(option <= STYLE_UNDERLINE);
  
  char buffer[BUFSIZE];

  if( option == STYLE_NONE )
    {
      sprintf(buffer, "\033[%d;%dm%s\033[0m", 30+fg, 40+bg, str);
    }
  else
    {
      sprintf(buffer, "\033[%d;%d;%dm%s\033[0m", 30+fg, 40+bg, option, str);
    }

  write(1, buffer, strlen(buffer));

  // reset stdout and stdin colors
  write(2,"\033[0m",5);
  write(0,"\033[0m",5);
}

// display a board with some colors
void board_color_display(struct board const* self)
{
  assert(self);

  // for each tiles and borders
  for(int i=-1; i<BOARD_HEIGHT+1; i++)
    {
      for(int j=-1; j<BOARD_WIDTH+1; j++)
	{
	  // borders
	  if((i == -1 || i == BOARD_HEIGHT) && (j == -1 || j == BOARD_WIDTH))
	    {
	      print_color("   ", COLOR_RED, COLOR_YELLOW, STYLE_NONE);
	    }
	  if( (i == -1 || i == BOARD_HEIGHT) && !(j == -1 || j == BOARD_WIDTH) )
	    {
	      char tmp[BUFSIZE];
	      sprintf(tmp, " %d ", j);
	      
	      print_color(tmp, COLOR_RED, COLOR_YELLOW, STYLE_NONE);
	    }
	  if( (j == -1 || j == BOARD_WIDTH) && !(i == -1 || i == BOARD_HEIGHT) )
	    {
	      char tmp[BUFSIZE];
	      sprintf(tmp, " %c ", i + 'A');
	      
	      print_color(tmp, COLOR_RED, COLOR_YELLOW, STYLE_NONE);

	    }

	  // tiles
	  if( j > -1 && j < BOARD_WIDTH && i > -1 && i < BOARD_HEIGHT )
	    {
	      int fg = COLOR_BLACK;

	      if( self->data[i][j].content == CONTENT_WATER )
		{
		  fg = COLOR_BLUE;
		}
	      else if( self->data[i][j].team == 0 )
		{
		  fg = COLOR_GREEN;
		}
	      else if(self->data[i][j].team == 1)
		{
		  fg = COLOR_RED;
		}
	  
	      char tmp[BUFSIZE];
	      sprintf(tmp, " %c ", display_rule[self->data[i][j].content]);
	      
	      print_color(tmp, COLOR_BLACK, fg, STYLE_NONE);
	    }
	}
      write(1, "\n",2);
    }

  write(1, "\n",2);
}


/* UTILITIES */

// remove the content of a tile (CONTENT_XXX to CONTENT_NONE)
void reset_tile(struct board* board,int i,int j)
{
  assert(board);
  assert(i >= 0);
  assert(i < BOARD_HEIGHT);
  assert(j >= 0);
  assert(j < BOARD_WIDTH);
  
  board->data[i][j].content = CONTENT_NONE;
  board->data[i][j].team = -1;

  // update the probabilities for this tile
  for(int k=0; k<PIECE_COUNT; k++)
    {
      board->data[i][j].probabilities[k] = 0.0;
    }
}

// return a random number greater than a and lower than b - 1
int random(int a, int b)
{
  assert( a >= 0 );
  assert( b > a );
  
  return rand()%(b-a)+a;
}

// return the the number of microseconds since the epoch
suseconds_t get_microseconds()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);

  return tv.tv_sec*1000000 + tv.tv_usec;
}

// if att win the battle versus def, return n>0
// if att lose the battle versus def, return n<0
// if it leads to a draw, return n=0
// ( similar to strcmp )
int piece_cmp(int att, int def)
{
  assert(att <= CONTENT_SPY);
  assert(att >= CONTENT_MARSHAL);
  assert(def <= CONTENT_BOMB);
  assert(def >= CONTENT_MARSHAL);

  if(att == def)
    {
      return 0;
    }
    
  if(att == CONTENT_SPY && def == CONTENT_MARSHAL)
    {
      return 1;
    }

  if(att == CONTENT_MINESWEEPER && def == CONTENT_BOMB)
    {
      return 1;
    }
  
  if(def == CONTENT_BOMB)
    {
      return -1;
    }

  if(def == CONTENT_FLAG)
    {
      return 1;
    }

  return def - att; 
}

// place the content owned by num_player at the coordinates (i; j) in a board
int place_one_content(int num_player, struct board* board, int content, int i, int j)
{
  assert(board);
  assert(num_player == 0 || num_player == 1);
  assert(content >= CONTENT_MARSHAL);
  assert(content <= CONTENT_UNKNOWN);
  
  if( !pos_is_valid(board, i, j) )
    {
      fprintf(stderr, "E: cant place %d at position (%d, %d) (invalid) !\n", content, i, j);
      return 0;
    }

  if( board->data[i][j].content != CONTENT_NONE )
    {
      fprintf(stderr, "E: cant place %d, location (%d, %d) already used !\n", content, i, j);
      return 0;
    }
    
  board->data[i][j].team = num_player;
  board->data[i][j].content = content;
  
  return 1;
}

// check if the initial board of the player num_player fit with the rules
int check_place(int num_player, struct board* board)
{
  int quantity[PIECE_COUNT];

  // init count to 0
  for(int i=0; i<PIECE_COUNT;i++)
    {
      quantity[i] = 0;
    }
  
  // count pieces and check if they are at the right place
  for(int i=0; i < BOARD_HEIGHT; i++)
    {
      for(int j=0; j < BOARD_WIDTH; j++)
	{
	  if( board->data[i][j].content >= CONTENT_MARSHAL &&  board->data[i][j].content <= CONTENT_BOMB )
	    {
	      // check player's row
	      if( !(j >= 0 + num_player * 6 && j<= 3 + num_player * 6) )
		{
		  fprintf(stderr, "E: Wrong row !\n");
		  return 0;
		}
	    }
	  
	  quantity[board->data[i][j].content]++;
	}
    }

  // check if the quantity rules are followed
  for(int i=0; i<PIECE_COUNT; i++)
    {
      if( quantity[i] != quantity_rule[i] )
	{
	  fprintf(stderr, "E: Bad count !\n");
	  return 0;
	}
    }
  
  return 1;
}

// return 1 if the tile at the coordinates (i; j) belongs to the board
int exists(int i, int j)
{
  return ( i >= 0 && i < BOARD_HEIGHT && j>=0 && j <BOARD_WIDTH );
}

// return 1 if the tile (i;j) can potentially welcome a soldier
int pos_is_valid(struct board const* self, int i, int j)
{
  assert(self);
  
  if( !exists(i, j) )
    {
      return 0;
    }

  return ( self->data[i][j].content != CONTENT_WATER );
}

// return 1 if the player can move the soldier from (i;j) with a direction dir and a distance dist
// on the board self
int move_is_valid(int player, struct board const* self,  int i, int j, char dir, int dist)
{
  assert(self);
  assert(player == 0 || player == 1);
  
  int dest_i = 0;
  int dest_j = 0;

  get_destination(i, j, &dest_i, &dest_j, dir, dist);
    
  // position of the source is invalid
  if( !pos_is_valid(self, i, j) || !pos_is_valid(self, dest_i, dest_j) )
    {
      if( DEBUG_MODE ){ fprintf(stderr, "MOVE_IS_VALID position not valid: %c %d %c %d\n", i + 'A', j, dir, dist);}
      return 0;
    }

  // not a real movement
  if( dist == 0 )
    {
      if( DEBUG_MODE ){ fprintf(stderr, "MOVE_IS_VALID not a real movement: %c %d %c %d\n", i + 'A', j, dir, dist);}
      return 0;
    }
  
  // the movement is a diagonal
  if( i != dest_i && j != dest_j )
    {
      if( DEBUG_MODE )
	{
	  fprintf(stderr, "MOVE_IS_VALID attempt diagonal move: %c %d %c %d\n", i + 'A', j, dir, dist);
	}
      return 0;
    }
  
  // the content of the source is not movable (flags, bombs, none and other player's things)
  // or destination not reachable
  if( self->data[i][j].content == CONTENT_FLAG
      || self->data[i][j].content == CONTENT_BOMB
      || self->data[i][j].team != player
      || self->data[i][j].content == CONTENT_NONE
      || self->data[dest_i][dest_j].team == player )
    {
      if( DEBUG_MODE ){ fprintf(stderr, "MOVE_IS_VALID move a bomb, a flag, another player or an empty tile : %c %d %c %d\n", i + 'A', j, dir, dist);}
      return 0;
    }

  // the distance is not valid
  if( dist > 1 && self->data[i][j].content != CONTENT_UNKNOWN && self->data[i][j].content != CONTENT_SCOUT )
    {
      if( DEBUG_MODE ){ fprintf(stderr, "MOVE_IS_VALID move too far: %c %d %c %d\n" ,i + 'A', j, dir, dist);}
      return 0;
    }

  int move_i = ( j == dest_j ) ? 1 : 0;
  int move_j = ( i == dest_i ) ? 1 : 0;

  if( dir == NORTH ) move_i *= -1;
  if( dir == WEST  ) move_j *= -1;

  // make sur that all tiles from source to destination are  reachable
  for(int k = 1; k < dist; k++)
    {
      int new_i = move_i * k + i;
      int new_j = move_j * k + j;

      if( !pos_is_valid(self, new_i, new_j) )
	{
	  if( DEBUG_MODE ){ fprintf(stderr, "MOVE_IS_VALID not reachable destination : %c %d %c %d\n", i + 'A', j, dir, dist);}
	  return 0;
	}
      
      if( self->data[new_i][new_j].content != CONTENT_NONE )
	{
	  if( DEBUG_MODE ){ fprintf(stderr, "MOVE_IS_VALID destination already filled : %c %d %c %d\n", i + 'A', j, dir, dist);}
	  return 0;
	}
    }

  return 1;
}

// move the content from one tile to an other
int move_one_content(int num_player, struct board* board, int i, int j, char dir, int dist)
{
  assert(board);
  assert(num_player == 0 || num_player == 1);

  // this move is not valid
  if( !move_is_valid(num_player,board ,i, j,dir, dist) )
    {
      if( DEBUG_MODE )
	{
	  fprintf(stderr, "MOVE_ONE_CONTENT move not valid: %c %d %c %d\n", i + 'A', j, dir, dist);
	}
      return 0;
    }
  
  int dest_i = 0;
  int dest_j = 0;

  get_destination(i, j,&dest_i, &dest_j, dir, dist);

  // invalid move (move on your own troops)
  if( board->data[dest_i][dest_j].team + num_player == 1 )
    {
      if( DEBUG_MODE ){ fprintf(stderr, "MOVE_ONE_CONTENT move on own troops: %c %d %c %d\n", i + 'A', j, dir, dist);}
      return 0;
    }

  // move to an invalid place
  if( !place_one_content(num_player, board, board->data[i][j].content, dest_i, dest_j) )
    {
      if( DEBUG_MODE ){ fprintf(stderr, "MOVE_ONE_CONTENT move on an invalid place: %c %d %c %d\n", i + 'A', j, dir, dist);}
      return 0;
    }

  // make the probabilities follow the content
  for(int k=0; k<PIECE_COUNT; k++)
    {
      board->data[dest_i][dest_j].probabilities[k] = board->data[i][j].probabilities[k];
      board->data[i][j].probabilities[k] = 0.0;
    }
  
  reset_tile(board, i, j);

  return 1;
}

// compute the coordinate of a tile starting from an other tile, a direction and a distance
void get_destination(int old_i, int old_j, int* i, int* j, char dir, int dist)
{
  assert(i);
  assert(j);

  *i = 0;
  *j = 0;
  
  switch( dir )
    {
    case NORTH: *i = -1; break;
    case SOUTH: *i = 1; break;
    case WEST: *j = -1; break;
    case EAST: *j = 1; break;
    }
  
  *i *= dist;
  *j *= dist;

  *i += old_i;
  *j += old_j;
}

/* INPUTS/OUTPUTS */

// input

// read one line in stdin and store it in str
// cf read_one_line_in
int read_one_line(char* str)
{
  assert(str);
  return read_one_line_in(str, 0);
}

// read one line in the file descriptor fd and store it in str
int read_one_line_in(char* str, int fd)
{
  assert(str);
  
  char buffer;
  ssize_t size = 0;
  int i = 0;

  while( read(fd, &buffer, 1) != 0 )
    {
      size ++;

      if( size > BUFSIZE )
	{
	  fprintf(stderr, "E: buffer overflow ! max: %d\n", BUFSIZE);
	  return 0;
	}

      if(buffer == '\n')
	{
	  break;
	}

      str[i] = buffer;

      ++i;
    }

  // make str a string
  str[size-1] = '\0';
  
  return 1;
}

// output

// write the string what in stdout
// cf write_one_line
int write_out(char* what)
{
  assert(what);
  return write_one_line(what, 1);
}

// write the string what in stderr
// cf write_one_line
int write_err(char* what)
{
  assert(what);
  return write_one_line(what, 2);
}

// write the string what in the file descriptor where
int write_one_line(char* what, int where)
{
  assert(what);
  if( !write(where, what, strlen(what)) )
    {
      fprintf(stderr, "E: Can't write in pipe %d !\n", where);
      return 0;
    }

  return 1;
}
