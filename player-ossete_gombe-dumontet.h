#ifndef PLAYER_H
#define PLAYER_H

#include <stdlib.h>
#include <stdio.h>

struct game_state
{
  int my_pieces[PIECE_COUNT];
  
  int his_pieces[PIECE_COUNT];
  int pieces_count;
  int discovered_pieces[PIECE_COUNT];
  
  struct board my_board;
};

/* PLACEMENT */
int place_enemy(int num_player, struct game_state *game);
int place(int num_player, struct game_state *game);
void send_place(int num_player, struct board *board);

/* STRATEGY AND EVALUATION */
int evaluate(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_default(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_marshal(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_general(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_colonel(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_commander(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_captain(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_lieutenant(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_sergent(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_minesweeper(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_scout(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);
int evaluate_spy(int num_player , struct game_state *game , int i ,int j ,char dir, int dist);

int next_step_debug(int num_player, struct game_state *game, char* res);
int next_step(int num_player, struct game_state *game, char* res);
float count_discovered(int num_player, struct game_state *game, int i_start, int j_start, char dir, int dist, int research );
int avoid(int num_player, struct game_state* game, int *avoids, float *threshold, int *danger_valuation, size_t nb_avoids, int i, int j, int new_i, int new_j);
int hunt(int num_player , struct game_state *game, int i, int j, char dir,
	 int dist, int* targets, size_t nb_targets, int hunt_radius, float threshold);


/* MSG */
void cant_read();
void cant_play(int num_player);

/* TURN */
int my_turn(int num_player, struct game_state *game);
int his_turn(int num_player, struct game_state *game);
void probabilities_update(int num_player, struct game_state *game);

#endif
