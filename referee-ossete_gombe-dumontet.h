#ifndef REFEREE_H
#define REFEREE_H
#define DEBUG_MODE_REFEREE 0
#define TURN_TIME_MAX 500000
#define TURN_TIME_DISPLAY 1000000

int get_pieces_from_one_player(int num_player, struct board* board, int p);
int get_step(int* i, int *j, char* dir, int* dist, int p);
int player_turn(int num_player, struct board *board, int pla_input, int pla_output, int adv_input, int* victory);


#endif
